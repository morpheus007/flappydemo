package com.myflappy.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.myflappy.game.FlappyDemo;
import com.myflappy.game.sprites.Bird;
import com.myflappy.game.sprites.Ground;
import com.myflappy.game.sprites.Tube;

public class PlayState extends State {
    private static final int TUBE_SPACING = 125;
    private static final int TUBE_COUNT = 4;


    private Bird bird;
    private Texture bg;
    private Ground ground;


    private Array<Tube> tubes;

    public PlayState(GameStateManager gsm) {
        super(gsm);

        bird = new Bird(50, 300);
        ground = new Ground(gameCam.position.x - gameCam.viewportWidth / 2);

        gameCam.setToOrtho(false, FlappyDemo.WIDTH / 2, FlappyDemo.HEIGHT / 2);
        bg = new Texture("bg.png");

        tubes = new Array<Tube>();
        for (int i = 1; i <= TUBE_COUNT; i++) {
            tubes.add(new Tube(i * (TUBE_SPACING + Tube.TUBE_WIDTH)));
        }
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched())
            bird.jump();
    }

    @Override
    public void update(float dt) {
        handleInput();
        bird.update(dt);


        //update gameCam
        gameCam.position.x = bird.getPosition().x + 80;

        if (gameCam.position.x - (gameCam.viewportWidth / 2) > ground.getGroundPos1().x + ground.getGround().getWidth()) {
            ground.reposition(ground.getGroundPos1().x + ground.getGround().getWidth());
        }

        for (int i = 0; i < tubes.size; i++) {

            Tube tube = tubes.get(i);

            if (gameCam.position.x - (gameCam.viewportWidth / 2) > tube.getPosTopTube().x + tube.getTopTube().getWidth()) {
                tube.reposition(tube.getPosTopTube().x + (Tube.TUBE_WIDTH + TUBE_SPACING) * TUBE_COUNT);
            }
            if (tube.collides(bird.getBounds())) {
                gsm.set(new MenuState(gsm));
            }
        }
        if (bird.getPosition().y <= ground.getGround().getHeight() + Ground.GROUND_Y_OFFSET) {
            gsm.set(new MenuState(gsm));
        }
        gameCam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(gameCam.combined);
        sb.begin();
        sb.draw(bg, gameCam.position.x - gameCam.viewportWidth / 2, 0);
        sb.draw(bird.getTexture(), bird.getPosition().x, bird.getPosition().y);

        for (Tube tube : tubes) {
            sb.draw(tube.getTopTube(), tube.getPosTopTube().x, tube.getPosTopTube().y);
            sb.draw(tube.getBottomTube(), tube.getPosBottomTube().x, tube.getPosBottomTube().y);
        }

        sb.draw(ground.getGround(), ground.getGroundPos1().x, ground.getGroundPos1().y);
        sb.draw(ground.getGround(), ground.getGroundPos2().x, ground.getGroundPos2().y);

        sb.end();
    }

    @Override
    public void dispose() {
        bg.dispose();
        bird.dispose();
        for (Tube tube : tubes) {
            tube.dispose();
        }
        ground.dispose();
        System.out.println("PlayState disposed");
    }

}
