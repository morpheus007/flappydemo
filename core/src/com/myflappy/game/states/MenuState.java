package com.myflappy.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.myflappy.game.FlappyDemo;

public class MenuState extends State {
    private Texture background;
    private Texture playButton;

    public MenuState(GameStateManager gsm) {
        super(gsm);

        gameCam.setToOrtho(false, FlappyDemo.WIDTH / 2, FlappyDemo.HEIGHT / 2);

        background = new Texture("bg.png");
        playButton = new Texture("playbtn.png");

    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            gsm.set(new PlayState(gsm));
            dispose();
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {

        //set the camera projection
        sb.setProjectionMatrix(gameCam.combined);

        sb.begin();
        sb.draw(background, 0, 0);

        //The x axis divided by two but it will draw to the right side of the center so
        //we subtract the half width of the object to keep in center
        sb.draw(playButton, gameCam.position.x-playButton.getWidth()/2, gameCam.position.y);

        sb.end();
    }

    @Override
    public void dispose() {

        background.dispose();
        playButton.dispose();
        System.out.println("MenuState disposed");
    }
}
