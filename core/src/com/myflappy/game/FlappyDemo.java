package com.myflappy.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.myflappy.game.states.GameStateManager;
import com.myflappy.game.states.MenuState;

public class FlappyDemo extends ApplicationAdapter {
    //width and height of the game screen
    public static final int WIDTH = 480;
    public static final int HEIGHT = 800;
    public static String TITLE = "Fappy Bird";

    private GameStateManager gsm;

    //container which stores all the textures
    private static SpriteBatch batch;

    //Generate music
    private Music music;


    @Override
    public void create() {

        batch = new SpriteBatch();

        gsm=new GameStateManager();

        Gdx.gl.glClearColor(1, 0, 0, 1);
        gsm.push(new MenuState(gsm));

        //Play Music in app
        music=Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
        music.setLooping(true);
        music.setVolume(0.1f);
        music.play();
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gsm.update(Gdx.graphics.getDeltaTime());
        gsm.render(batch);
    }

    @Override
    public void dispose() {
        super.dispose();

        //dispose music after the application exits
        music.dispose();
    }

}
