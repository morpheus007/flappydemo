package com.myflappy.game.sprites;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class Ground {
    public static final int GROUND_Y_OFFSET = -50;
    private Texture ground;
    private Vector2 groundPos1, groundPos2;

    public Ground(float x) {

        ground = new Texture("ground.png");

        groundPos1 = new Vector2(x, Ground.GROUND_Y_OFFSET);
        groundPos2 = new Vector2(x + ground.getWidth(), Ground.GROUND_Y_OFFSET);
    }

    public Texture getGround() {
        return ground;
    }

    public Vector2 getGroundPos1() {
        return groundPos1;
    }

    public Vector2 getGroundPos2() {
        return groundPos2;
    }

    public void reposition(float x) {
       groundPos1.set(x, Ground.GROUND_Y_OFFSET);
        groundPos2.set(x + ground.getWidth(), Ground.GROUND_Y_OFFSET);

    }

    public void dispose() {
        ground.dispose();
    }
}
